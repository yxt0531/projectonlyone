extends Node

const STOCKS_JSON_FILE_LOCATION = "res://Res/Stocks.json"
const MARKET_SHUFFLE_INTERVAL = 0.5 # default 0.5
const RANDOM_PRICE_LOW = 0.01
const RANDOM_PRICE_HIGH = 2

var stocks = []
var market_state = false
var market_price_modifier = 0.05 # price change amount
var market_tendency = 0 # < 0 tends to go down, 0 tends to maintain, > 0 tends to go up
var time_counter = .0

func _ready():
	get_stocks_from_json()
	randomize_all_stock_price()

func _process(delta):
	if time_counter >= MARKET_SHUFFLE_INTERVAL && market_state == true:
		shuffle_market()
		time_counter = 0
	else:
		time_counter = time_counter + delta
	
func get_stocks_from_json():
	var json_file = File.new()
	json_file.open(STOCKS_JSON_FILE_LOCATION, File.READ)
	stocks = parse_json(json_file.get_as_text())
	print("Stock list successfully loaded from json with " + str(stocks.size()) + " stocks.")
	
	json_file.close()
	
func open_market():
	market_state = true
	
func close_market():
	market_state = false
	
func shuffle_market():
	var price_buffer
	
	for s in stocks: # s as each stock from stocks array
		s.history.append(s.price)
		
		for h in s.history:
			s.avg = s.avg + h
		s.avg = s.avg / s.history.size()
		
		randomize()
		
		if s.price >= 10 && s.price < 100: # price cap
			price_buffer = s.price * market_price_modifier * 0.1
			if market_tendency > 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer * (market_tendency + 1)), 0.01) # price tends to go up
			elif market_tendency == 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer), 0.01) # random price
			else:
				s.price = stepify(s.price + rand_range(price_buffer * (market_tendency - 1), price_buffer), 0.01) # price tends to go down
		elif s.price >= 100 && s.price < 1000: # price cap
			price_buffer = s.price * market_price_modifier * 0.01
			if market_tendency > 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer * (market_tendency + 1)), 0.01) # price tends to go up
			elif market_tendency == 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer), 0.01) # random price
			else:
				s.price = stepify(s.price + rand_range(price_buffer * (market_tendency - 1), price_buffer), 0.01) # price tends to go down
		elif s.price >= 1000 && s.price < 9999:
			price_buffer = s.price * market_price_modifier
			s.price = stepify(s.price + rand_range(-price_buffer * 2, price_buffer), 0.01) # price tends to go down
		else: # normal mode
			price_buffer = s.price * market_price_modifier
			if market_tendency > 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer * (market_tendency + 1)), 0.01) # price tends to go up
			elif market_tendency == 0:
				s.price = stepify(s.price + rand_range(-price_buffer, price_buffer), 0.01) # random price
			else:
				s.price = stepify(s.price + rand_range(price_buffer * (market_tendency - 1), price_buffer), 0.01) # price tends to go down

		if s.price < 0.01:
			s.price = 0.01 # prevent 0 dollar stock
		elif s.price > 9999:
			s.price = 9999 # prevent 9999 dollar stock
			
func set_market_tendency(value):
	market_tendency = value

func set_market_modifier(value):
	market_price_modifier = value
	
func set_market_state(state):
	market_state = state

func set_stock_price(index, value):
	stocks[index].price = value
	
func get_stock_name(index):
	return stocks[index].name	
	
func get_stock_current(index):
	return stocks[index].price	
	
func get_stock_avg(index):
	return stocks[index].avg
	
func get_history_low(index):
	return stocks[index].history.min();
	
func get_history_high(index):
	return stocks[index].history.max();
	
func randomize_all_stock_price():
	for s in stocks:
		randomize()
		s.price = stepify(rand_range(RANDOM_PRICE_LOW, RANDOM_PRICE_HIGH), 0.01)