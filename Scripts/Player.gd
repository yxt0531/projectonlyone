extends Node

var balance = 1 # default 1
var stock_owned = []

func buy_stock(index):
	if balance >= $"/root/Stocks".get_stock_current(index):
		balance = balance - $"/root/Stocks".get_stock_current(index)
		stock_owned.append(index)
	
func sell_stock(index):
	stock_owned.remove(stock_owned.find(index, 0))
	balance = balance + $"/root/Stocks".get_stock_current(index)
	
func get_player_balance():
	return balance
	
func set_player_balance(value):
	balance = value
	
func get_player_stock_ownership(index):
	return stock_owned.count(index)
	
func get_player_status(): # debugging only
	print("Currently Owned:" + str(stock_owned) + "; Balance: " + str(balance))