extends Node2D

const AVERAGE_UPDATE_INTERVAL = 2 # update daily average per second

export var index = 0 # stock index

var lowest_day = .0
var highest_day = .0
var history_day = []
var avg_day = .0

var time_counter = .0

func _ready():
	$Name.text = $"/root/Stocks".get_stock_name(index)
	
	lowest_day = $"/root/Stocks".get_stock_current(index)
	$LowestDay.text = str(lowest_day)
	highest_day = $"/root/Stocks".get_stock_current(index)
	$HighestDay.text = str(highest_day)
	
	$LowestTotal.text = str($"/root/Stocks".get_history_low(index))
	$HighestTotal.text = str($"/root/Stocks".get_history_high(index))
	$AvgTotal.text = str($"/root/Stocks".get_stock_avg(index))
	
func _process(delta):
	$Current.text = str($"/root/Stocks".get_stock_current(index)) # update current price
	
	if $"/root/Stocks".get_stock_current(index) > highest_day: # update highest / lowest day price
		highest_day = $"/root/Stocks".get_stock_current(index)
		$HighestDay.text = str(highest_day)
	if $"/root/Stocks".get_stock_current(index) < lowest_day:
		lowest_day = $"/root/Stocks".get_stock_current(index)
		$LowestDay.text = str(lowest_day)
	history_day.append($"/root/Stocks".get_stock_current(index))
	
	if time_counter >= AVERAGE_UPDATE_INTERVAL:
		for p in history_day: # update average day price
			avg_day = avg_day + p
		avg_day = avg_day / history_day.size()
		$AvgDay.text = str(avg_day)
		time_counter = .0
	else:
		time_counter = time_counter + delta

	if $"/root/Player".get_player_balance() < $"/root/Stocks".get_stock_current(index): # balance check
		$Buy.disabled = true
	else:
		$Buy.disabled = false
	
	if $"/root/Player".get_player_stock_ownership(index) <= 0 && $"/root/Player".get_player_stock_ownership(index) < 9999: # ownership check
		$Sell.disabled = true
		$Sell.text = "Sell"
	elif $"/root/Player".get_player_stock_ownership(index) >= 9999:
		$Buy.disabled = true
		$Sell.disabled = false
		$Sell.text = str($"/root/Player".get_player_stock_ownership(index))
	else:
		$Sell.disabled = false
		$Sell.text = str($"/root/Player".get_player_stock_ownership(index))
	
func _on_Buy_pressed():
	if Input.is_action_pressed("buy_bulk"):
		while $"/root/Player".get_player_balance() >= $"/root/Stocks".get_stock_current(index) && $"/root/Player".get_player_stock_ownership(index) < 9999:
			$"/root/Player".buy_stock(index)
	else:
		$"/root/Player".buy_stock(index)

func _on_Sell_pressed():
	if Input.is_action_pressed("buy_bulk"):
		while $"/root/Player".get_player_stock_ownership(index) > 0:
			$"/root/Player".sell_stock(index)
	else:
		$"/root/Player".sell_stock(index)
