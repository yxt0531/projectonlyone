extends Node

const STOCKS_JSON_FILE_LOCATION = "res://Res/RandomStories.json"

var current_day = 0 # default 0
var current_time = 0 # default 0
var time_buffer = 0

var random_story = []

func _ready():
	get_stories_from_json()

func get_stories_from_json():
	var json_file = File.new()
	json_file.open(STOCKS_JSON_FILE_LOCATION, File.READ)
	random_story = parse_json(json_file.get_as_text())
	print("Random story list successfully loaded from json with " + str(random_story.size()) + " stories.")
	
	json_file.close()
	
func _process(delta):
	time_buffer = time_buffer + delta
	if time_buffer >= 1:
		current_time = current_time + 1
		time_buffer = 0

func new_day(): # progress to next day
	current_day = current_day + 1
	current_time = 0

func get_story(): # story script
	if current_day == 1: # get day 1 story
		if current_time == 1:
			$"/root/Stocks".set_market_modifier(0.05);
		elif current_time == 10:
			return get_random_story()
		elif current_time == 20:
			return get_random_story()
		elif current_time == 30:
			current_time = current_time + 1
			$"/root/Stocks".set_market_tendency(1.5)
			return "The government just announced its new plan to vitalize the stock market.\n\n"
		elif current_time == 40:
			current_time = current_time + 1
			$"/root/Stocks".set_market_tendency(0)
		elif current_time == 50:
			return get_random_story()
	elif current_day == 2: # get day 2 story
		if current_time == 1:
			$"/root/Stocks".set_market_modifier(0.075);
		elif current_time == 10:
			return get_random_story()
		elif current_time == 20:
			return get_random_story()
		elif current_time == 30:
			current_time = current_time + 1
			$"/root/Stocks".set_market_tendency(2)
			return "Experts says large amount of cash were poured into the market in only 30 seconds.\n\n"
		elif current_time == 40:
			current_time = current_time + 1
			$"/root/Stocks".set_market_tendency(0)
		elif current_time == 50:
			return get_random_story()
	elif current_day == 3: # get day 3 story
		if current_time == 1:
			$"/root/Stocks".set_market_modifier(0.1);
		elif current_time == 10:
			return get_random_story()
		elif current_time == 20:
			return get_random_story()
		elif current_time == 30:
			current_time = current_time + 1
			$"/root/Stocks".set_market_tendency(-1)
			return "A group of hackers claimed that roughly 3 million accounts\' information were stolen from the stock market exchange center.\n\n"
		elif current_time == 40:
			return get_random_story()
		elif current_time == 50:
			return get_random_story()
	elif current_day == 4: # get day 4 story
		if current_time == 1:
			$"/root/Stocks".set_market_modifier(0.15)
		elif current_time == 10:
			return get_random_story()
		elif current_time == 20:
			return get_random_story()
		elif current_time == 30:
			current_time = current_time + 1
			$"/root/Stocks".set_stock_price(14, $"/root/Stocks".get_stock_current(14) * rand_range(1, 2))
			return "White Raven Works announced their new game \"Simple Little Wish\"."
		elif current_time == 40:
			return get_random_story()
		elif current_time == 50:
			return get_random_story()
	elif current_day == 5: # get day 5 story
		if current_time == 1:
			$"/root/Stocks".set_market_modifier(0.2);
		elif current_time == 10:
			$"/root/Stocks".set_market_tendency(2)
			return get_random_story()
		elif current_time == 20:
			return get_random_story()
		elif current_time == 30:
			return get_random_story()
		elif current_time == 40:
			return get_random_story()
		elif current_time == 50:
			return get_random_story()
			
	else: # after main story
		if current_time == 1:
			randomize()
			$"/root/Stocks".set_market_modifier(rand_range(0.025, 0.075))
			randomize()
			$"/root/Stocks".set_market_tendency(rand_range(-2, 2))
		elif current_time == 20:
			randomize()
			$"/root/Stocks".set_market_modifier(rand_range(0.025, 0.075))
			randomize()
			$"/root/Stocks".set_market_tendency(rand_range(-2, 2))
		elif current_time == 40:
			randomize()
			$"/root/Stocks".set_market_modifier(rand_range(0.025, 0.075))
			randomize()
			$"/root/Stocks".set_market_tendency(rand_range(-2, 2))
			
	return ""

func get_random_story():
	if random_story.size() == 0: # no more random story
		return ""
	
	randomize()
	current_time = current_time + 1
	var index = randi() % random_story.size()
	var story = random_story[index] + "\n"
	random_story.remove(index)
	return story

func get_current_day():
	return current_day