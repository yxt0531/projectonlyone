extends Node2D

const MARKET_DUATION = 60

var time_counter = .0

func _ready():
	$"/root/Stocks".open_market() # resume market process
	$"/root/Stories".new_day() # progress to next day

func _process(delta):
	time_counter = time_counter + delta
	if time_counter >= MARKET_DUATION: # end day after reach max market duration
		$"/root/Stocks".close_market()
		get_tree().change_scene("res://Scenes/DailySummary.tscn")

	$StaticLabel/Balance.text = str($"/root/Player".get_player_balance()) # update balance
	$Storyboard.append_bbcode($"/root/Stories".get_story()) # update story