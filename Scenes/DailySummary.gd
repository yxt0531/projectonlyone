extends Node2D

func _ready():
	$"/root/Stocks".set_market_state(false) # stop market from running
	$"/root/Stocks".set_market_modifier(0.05) # reset market modifier
	$"/root/Stocks".set_market_tendency(0) # reset market tendency
	
	$Day.text = "Day: " + str($"/root/Stories".get_current_day())
	$Balance.text = "Balance: " + str($"/root/Player".get_player_balance())

	if $"/root/Stories".get_current_day() == 5:
		$Exit.visible = true
		$Day.text = "Congrats!"
		$Balance.text = "You've earned " + str($"/root/Player".get_player_balance()) + " CR."
		$Question.visible = true
	else:
		$Question.visible = false
	
	if $"/root/Stories".get_current_day() > 5:
		$Exit.visible = true

func _on_Continue_pressed():
	get_tree().change_scene("res://Scenes/Trading.tscn")

func _on_Exit_pressed():
	get_tree().change_scene("res://Scenes/Credit.tscn")
